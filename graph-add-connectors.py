import requests
import json
import argparse

GRAPH_HOST = "127.0.0.1"
GRAPH_PORT = 7200

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--node', type=int)
args = parser.parse_args()

id = f"graph-node-{args.node}"

# ids = ["graph-node1","graph-node2","graph-node3","graph-node4","graph-node5","graph-node6","graph-node7","graph-node8","graph-node9"]
# for id in ids:
urlQuery = f"http://{GRAPH_HOST}:{GRAPH_PORT}/repositories/{id}/statements"
query_1 = """
PREFIX :<http://www.ontotext.com/connectors/lucene#>
PREFIX inst:<http://www.ontotext.com/connectors/lucene/instance#>
INSERT DATA {
    inst:starwars_fts :createConnector '''
{
  "fields": [
    {
      "fieldName": "fts",
      "propertyChain": [
        "$literal"
      ],
      "indexed": true,
      "stored": true,
      "analyzed": true,
      "multivalued": true,
      "ignoreInvalidValues": false,
      "facet": false
    }
  ],
  "languages": [],
  "types": [
    "$untyped"
  ],
  "readonly": false,
  "detectFields": false,
  "importGraph": false,
  "skipInitialIndexing": false,
  "boostProperties": [],
  "stripMarkup": false
}
''' .
}
"""
query_2 = """
PREFIX :<http://www.ontotext.com/connectors/lucene#>
PREFIX inst:<http://www.ontotext.com/connectors/lucene/instance#>
INSERT DATA {
    inst:starwars_fts2 :createConnector '''
{
  "fields": [
    {
      "fieldName": "fts",
      "fieldNameTransform": "predicate.localName",
      "propertyChain": [
        "$literal"
      ],
      "indexed": true,
      "stored": true,
      "analyzed": true,
      "multivalued": true,
      "ignoreInvalidValues": false,
      "facet": false
    }
  ],
  "languages": [],
  "types": [
    "$untyped"
  ],
  "readonly": false,
  "detectFields": false,
  "importGraph": false,
  "skipInitialIndexing": false,
  "boostProperties": [],
  "stripMarkup": false
}
''' .
}"""
queries = [query_1, query_2]
for query in queries:
    params = {"update": query, "infer": True, "sameAs": True}
    headers = {"Accept": "application/sparql-results+json"}
    res = requests.post(urlQuery, params=params, headers=headers)
    print(id, res.status_code)

