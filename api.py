import os
import yaml
import flask
from flask import Flask, request, json, jsonify, after_this_request, send_file, abort
from flask_restx import Resource, Api, fields
from flask_restx.api import Swagger
from werkzeug.utils import safe_join
import agent_model

app = Flask(__name__)
app.config["DEBUG"] = True

api = Api(app=app,
    doc='/docs',
    version='1.0.0',
    contact='nadar@eurecom.fr',
    default_swagger_filename="IAKM-API-swagger.json",
    title='IAKM-API',
    description='IAKM-API is a RESTful API services, it consists of a various of endpoints that should be used by NDN nodes in order to manged their content stores.')

# @api.route('/swagger')
# class Swagger(Resource):
#     def get(self):
#         data = json.loads(json.dumps(api.__schema__))
#         with open('yamldoc.yml', 'w') as yamlf:
#             yaml.dump(data, yamlf, allow_unicode=True, default_flow_style=False)
#             file = os.path.abspath(os.getcwd())
#             try:
#                 @after_this_request
#                 def remove_file(resp):
#                     try:
#                         os.remove(safe_join(file, 'yamldoc.yml'))
#                     except Exception as error:
#                        print("Error removing or closing downloaded file handle", error)
#                     return resp
#                 return send_file(safe_join(file, 'yamldoc.yml'),
#                                  as_attachment=True,
#                                  download_name='yamldoc.yml',
#                                  mimetype='application/x-yaml')
#             except FileExistsError:
#                 abort(404)

@api.route('/JsonToInterest')
class JsonToInterest(Resource):
    def get(self):
        req_json = json.loads(request.data)
        interest = agent_model.loadTopic(req_json)
        return interest

@api.route('/InterestToJson')
class InterestToJson(Resource):
    def get(self):
        req_json = json.loads(request.data)
        Json = agent_model.dumpTopic(req_json["interest"])
        return Json

@api.route('/model')
class Model(Resource):
    def get(self):
        if 'context' in request.args:
            context = request.args['context']
            # /context_map/entity=roundabout/context_model/ml_type=decision/input=3/feature=1
            json_context = agent_model.topicToContext(context)
            aModel = agent_model.AgentModel(json_context)
            model = aModel.find_model()
            response = flask.make_response(model)
            return response
    def post(self):
        data = request.data
        if 'context' in request.args:
            context = request.args['context']
            # /context_map/entity=roundabout/context_model/ml_type=decision/input=3/feature=1
            json_context = agent_model.topicToContext(context)
            aModel = agent_model.AgentModel(json_context, data)
            res = aModel.save_model()
        return res
    def delete(self):
        if 'context' in request.args:
            context = request.args['context']
            # /context_map/entity=roundabout/context_model/ml_type=decision/input=3/feature=1
            json_context = agent_model.topicToContext(context)
            aModel = agent_model.AgentModel(json_context)
            deleted_data = aModel.delete_model()
            return deleted_data

@api.route('/graph/size')
class GraphSize(Resource):
    def get(self):
        if 'id' in request.args:
            return agent_model.get_graph_size(request.args['id'])
        else:
            return agent_model.get_graph_size()

@api.route('/graphs')
class Graphs(Resource):
    def get(self):
        return agent_model.get_graph()

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)