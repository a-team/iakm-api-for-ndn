import time
import requests
import json
import pickle
import traceback

def dictToTopic(dict, t=""):
    keys = list(dict.keys())
    values = list(dict.values())
    for i in range(len(keys)):
        if type(values[i]) == str:
            if t == "":
                t = keys[i] + "=" + str(values[i])
            else:
                t = t + "/" + keys[i] + "=" + str(values[i])
        else:
            t = t + "/" + keys[i]
            t = dictToTopic(values[i], t)
    return t.replace('/context/', '/')

context = { "graph_id" : "1",
            "context_map": {
                "entity": "roundabout"
            },
            "context_model": {
                "ml_type": "decision",
                "input": "3",
                "feature": "1"
            }
           }
my_str = "Model entering roundabout for ego vehicle in rondpoint eucalyptus"
params = {"context": dictToTopic(context)}

# ###################################### POST ########################################
# data = pickle.dumps(my_str)
# res = requests.post("http://127.0.0.1:5000/model",data=data, params=params)
# print(res)
# ##################################### GET #########################################
# params = {"context": dictToTopic(context)}
# res = requests.get("http://127.0.0.1:5000/model", params=params, )
# response_bytes = res.content
# data = pickle.loads(response_bytes)
# print(res.status_code, data)
# print("---------------------------------------------------------------------------")
# #################################### DELETE ########################################
params = {"context": dictToTopic(context)}
print(params)
res = requests.delete("http://127.0.0.1:5000/model", params=params)
print(res.status_code)
print(res.content.decode())
print("---------------------------------------------------------------------------")
#############################  json-to-interest   ####################################
res = requests.get("http://127.0.0.1:5000/JsonToInterest", data = json.dumps(context))
print(res.status_code)
print(res.content.decode()) # graph_id=1/context_map.entity=roundabout/context_model.ml_type=decision/context_model.input=3/context_model.feature=1/
print("---------------------------------------------------------------------------")
###########################  interest-to-json   ####################################
interest = {"interest":"graph_id=1/context_map.entity=roundabout/context_model.ml_type=decision/context_model.input=3/context_model.feature=1/"}
res = requests.get("http://127.0.0.1:5000/InterestToJson", data = json.dumps(interest))
print(res.status_code)
print(res.content.decode())
#####################################################################################








# try:
#     query = """
#                             PREFIX dc:   <http://purl.org/dc/elements/1.1/>
#                             PREFIX iakm: <http://www.example.org/IAKM.owl#>
#                             DELETE WHERE{
#                                 ?s1 ?p11 iakm:Roundabout_20231108155616074141 .
#                                 iakm:Roundabout_20231108155616074141 ?p12 ?o1 .
#                                 ?s2 ?p21 iakm:model_Roundabout_20231108155616074141 .
#                                 iakm:model_Roundabout_20231108155616074141 ?p22 ?o2 .
#                                 }
#  """
#
#     url = f"http://127.0.0.1:7200/repositories/graph-node-3/statements"
#     params = {"update": query, "infer": True, "sameAs": True}
#     headers = {"Accept": "application/sparql-results+json"}
#     res = requests.post(url, params=params, headers=headers)
#
#     if res.status_code == 200:
#         content = json.loads(res.content.decode())
#         print(content)
#     else:
#         print(res.status_code)
#         print(res.content)
# except:
#     print(traceback.format_exc(), flush=True)





