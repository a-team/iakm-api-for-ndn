# IAKM-API for NDN

IAKM-API is a RESTful API services, it consists of a various of endpoints that should be used by NDN nodes in order to manged their content stores.

## Intro

IAKM-API architecture consists of 3 containers:

- <strong>IAKM-API:</strong> represents the endpoint methods to receive client requests (designed for NDN content store calls) 
- <strong>IAKM-Graph: </strong> graphDB server to store metadata of AI models, By default it creates 9 graph instances with their map/model ontology each.
- <strong>IAKM-DB: </strong> NoSQL server (mongoDB) to store AI models, each models could be stored as file-system using the GridFS extension.

![Diagram](images/IAKM-API-NDN.png)

## Contents

*   [Deployement requirements](#deployement-requirements)
*   [Getting started](#getting-started)
    *   [IAKM-Graph](#iakm-graph)
    *   [IAKM-DB](#iakm-db)
    *   [IAKM-API](#iakm-api)
*   [Used Technologies](#used-technologies)
*   [Current improvement](#current-improvement)
*   [Contribute](#contribute)
*   [License](#license)

## Deployement requirements

*   [x] [Docker](https://docs.docker.com/engine/) Engine


## Getting Started

Clone the main branch of IAKM-API repository to your local machine:

```
git clone https://gitlab.eurecom.fr/a-team/iakm-api-for-ndn.git
```

After being dowloaded, run the main shell script "sh-build-run.sh":

```
./sh-build-run.sh
```

In the next sections, you can see more details concerning the steps taken in the main script.

### 1. IAKM Graph

As shown in the code below, IAKM-Graph can be depoyed by running 'khaller/graphdb-free:10.0.0' image, then we create the graph instances, the number of graph depends to the NDN's nodes number, where we create one graph for each NDN node. In our case, we are supposing NDN with 9 nodes,thus we create 9 ontologies using same graph configuration. Then we add for every graph an IAKM ontology which represent the conceptual classes for Map and Model contexts, and finally in this section, we add for every graph a Lucene engine search connector to handle the semantic search for AI models.

```bash
###################################### 1- run graphdb image ###################################################################
sudo docker run -d -p 127.0.0.1:7200:7200  --name iakm-graphdb -t khaller/graphdb-free:10.0.0                                 #
sleep 10 # wait to start GraphDB server                                                                                       #
# Loop for all graphs #########################################################################################################
for (( c=1; c<=9; c++ ))                                                                                                      #
do                                                                                                                            #
  new_graph="graph-node-$((c))"                                                                                               #
  #create repository                                                                                                          #
  sed -i "s/graph-node/$new_graph/g" graph-config.ttl                                                                         #
  curl -X POST http://127.0.0.1:7200/rest/repositories -H 'Content-Type: multipart/form-data' -F "config=@graph-config.ttl"   #
  sed -i "s/$new_graph/graph-node/g" graph-config.ttl                                                                         #
  # import graph data for Ontology ############################################################################################
  curl -X PUT  "http://127.0.0.1:7200/repositories/$new_graph/statements" \                                                   #
       -H 'Content-Type: application/x-trig' \                                                                                #
       -H 'Accept: application/json' \                                                                                        #
       -d @statements.trigs                                                                                                   #
  # add Lucene connectors #####################################################################################################
  python graph-add-connectors.py --node $c  # add Lucene search to graph                                                      #
done                                                                                                                          #
###############################################################################################################################
```

<!-- ![Alt Text](https://nextcloud.eurecom.fr/s/HjxZ6oywGZB7PMt/download/server.gif) -->
![Diagram](images/graphDB.png)


<br /><hr />
========================================================================================================================

### 2.IAKM-DB

IAKM-DB is a NoSQL database, in our case we use mongoDB that stores AI models in type of File-System, such that every model could be stored or retrieved using its unique ID :

```bash
###################################### 2- run mongodb image ###################################################################
sudo docker run -d -p 127.0.0.1:27017:27017 --expose 27017 --name iakm-mongodb -t mongo:3.6                                   #
###############################################################################################################################
```


<br /><hr />
========================================================================================================================

### 3. IAKM-API

IAKM-API exposes a set of endpoint methods that could be used to store or retrieve AI models according to their semantic context, the deployment referes to the 3rd part in the main script, where first we build the API's container, and then run the generated API image as below:

```bash
###################################### 3- IAKM-API  ###########################################################################
$ sudo docker build -t iakm_api .                                                                                             #
IP_GRAPH=$(sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' iakm-graphdb)                    #
IP_DB=$(sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' iakm-mongodb)                       #
sudo docker run -p 5000:5000 -e IP_GRAPH=${IP_GRAPH} -e IP_DB=${IP_DB} --name iakm-api iakm_api                               #
###############################################################################################################################
```

#### 3.1 Store-AI:
For instance, when an NDN node decides to store data in its content store, it send POST message to IAKM-API in order to store AI data according to its metdata.

<!-- ![Alt Text](https://nextcloud.eurecom.fr/s/BnqfPkrT2DWZnon/download/usecase-push.gif) -->


#### 3.2 Find-AI:
When an NDN node receives an interest, it should seek for a corresponding data on its content store, so it sends a GET message to IAKM-API in order to find AI that matchs the requested.

#### 3.3 Erase-AI:
NDN content store has a very small storage space, thus it could remain only the prioritized AI data, therefore it uses the DELETE message to remove AI from its contetn store. 


<br /><hr />
========================================================================================================================

### 2.4. API Endpoints
![Diagram](images/IAKM-API-swagger.png)

#### 2.4.1 /InteresetTojson /JsonToInterest:
![Diagram](images/ndn-convertors.png)

#### 2.4.2 /model:

In same way like NDN's Content Store, this method accept the 3 main requests:

- <strong>POST:</strong> equivalent to INSERT function in content store, requires as paramater the Context of AI model in json format and the AI model as data object of request.
- <strong>GET:</strong> equivalent to FIND function in content store, requires as paramater only the Context of AI model in json format and gives as response the AI model in bytes format.
- <strong>DELETE:</strong> equivalent to ERASE function in content store, requires as paramater only the Context of AI model in json format and gives as response some information about the deleted data.

![Diagram](images/model.png)

The image below shows how the context information stored in graphDB as triplets. 

![Diagram](images/ndn-graph-context.png)


#### 2.4.3 /graphs:
![Diagram](images/ndn-graphs.png)

#### 2.4.3 /graph/size:
![Diagram](images/ndn-graph-size.png)


## Used technologies

- <strong>Flask RESTful:</strong>to build API that represents the interface between the NDN node and its content store
- <strong>MongoDB/GridFS:</strong> For data storage of AI models 
- <strong>GraphDB:</strong> For data storage of AI metadata 
- <strong>Docker:</strong> helps for scalibility, portability and networking
  

## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
This work is released under Apache 2.0 license. See the [license file](LICENSE.txt) for more details. Contribution and integrations are appreciated.
