FROM python:3.7-slim

#FROM alpine:latest
#RUN apk update
#RUN apk add py-pip
#RUN apk add --no-cache python3-dev
#RUN pip install --upgrade pip
RUN apt-get update && apt-get install apt-transport-https && apt-get -y install vim
RUN apt-get install -y iputils-ping

WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt

RUN pip install PyYAML
RUN pip install requests
RUN pip install jsonschema
RUN pip install nltk
RUN pip install openai
RUN pip install jsonpath-ng

CMD python api.py
