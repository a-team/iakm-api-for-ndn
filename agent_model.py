import os
import json
import sys
import traceback
import jsonpath_ng
import ast
import pickle
from db import dbGateway

IP_DB, PORT_DB = os.environ['IP_DB'], 27017

def dictToTopic(dict, t=""):
    keys = list(dict.keys())
    values = list(dict.values())
    for i in range(len(keys)):
        if type(values[i]) == str:
            if t == "":
                t = keys[i] + "=" + str(values[i])
            else:
                t = t + "/" + keys[i] + "=" + str(values[i])
        else:
            t = t + "/" + keys[i]
            t = dictToTopic(values[i], t)
    return t.replace('/context/', '/')
def topicToContext(topic: str): # new 12/10/2023
    dict = {}
    context_map, context_model = {}, {}
    current_context = None
    try:
        arr_topic = topic.split('/')
        for prop in arr_topic:
            try:
                if '=' in prop:
                    arr_prop = prop.split('=')
                    if arr_prop[0] in ["graph_id","method", "model_usage", "agent_type", "trainability", "agent_id", "edge_id"]:
                        dict[arr_prop[0]] = arr_prop[1]
                    else:
                        current_context[arr_prop[0]] = arr_prop[1]
                else:
                    if prop == "context_map": current_context = context_map
                    if prop == "context_model": current_context = context_model
            except:pass
    except:pass


    dict["context_map"] = context_map
    dict["context_model"] = context_model
    return dict

def loadTopic(obj, prefix="", oldTopic=""):
    topic = oldTopic
    if isinstance(obj, dict):
        for key, value in obj.items():
            key = key.replace('/', '__slash__')
            if isinstance(value, list):
                if len(value) == 0:
                    topic += ("" if prefix == "" else f"{prefix}.") + ("{}={}/".format(key, "__list__"))
                else:
                    counter = 0
                    for item in value:
                        topic = loadTopic(item, (key if prefix == "" else f"{prefix}.{key}") + f".{counter}", topic)
                        counter += 1
            else:
                if isinstance(value, dict):
                    if value == {}:
                        topic += ("" if prefix == "" else f"{prefix}.") + ("{}={}/".format(key, "__dict__"))
                    else:
                        topic = loadTopic(value, (key if prefix == "" else f"{prefix}.{key}"), topic)
                else:
                    if isinstance(value, str): value = value.replace('/', '__slash__')
                    topic += ("" if prefix == "" else f"{prefix}.") + ("{}={}/".format(key, value))


    elif isinstance(obj, list):
        counter = 0
        for item in obj:
            topic = loadTopic(item, (f"{counter}" if prefix == "" else f"{prefix}.{counter}"), topic)
            counter += 1
    else:
        if isinstance(obj, str): obj = obj.replace('/', '__slash__')
        topic += ("" if prefix == "" else f"{prefix}={obj}/")
    return topic
def dumpTopic(topic):
    list_topic = topic.split('/')
    list_topic = [x for x in list_topic if len(x.strip()) > 3]
    obj = {}
    for sub_topic in list_topic:
        # region "solve value restriction"
        spliter = sub_topic.split('=')
        key = spliter[0]
        value = spliter[1]
        try:
            value = ast.literal_eval(value)
        except:
            value = value
        if value == "__dict__": value = {}
        if value == "__list__": value = []
        key = key.replace('__slash__', '/')
        if isinstance(value, str): value = value.replace('__slash__', '/')
        # endregion

        if '.' in key:
            try:
                value = ast.literal_eval(value)
            except:
                pass

            keys = key.split(".")
            temp = obj
            for i in range(len(keys)):  # [person, 0, name]
                try:
                    k = ast.literal_eval(keys[i])
                except:
                    k = keys[i]
                if isinstance(k, int): continue

                next_i = i + 1
                if next_i < len(keys):
                    try:
                        kk = ast.literal_eval(keys[next_i])
                    except:
                        kk = keys[next_i]
                    if isinstance(kk, int):
                        if next_i == len(keys) - 1:
                            jsonpath_expr = jsonpath_ng.parse(f'*.{k}[{kk}]')
                            res = jsonpath_expr.find(temp)
                            if len(res) == 0:
                                if k not in temp:
                                    temp[k] = [value]
                                else:
                                    temp[k].append(value)
                                temp = temp[k]
                        else:
                            jsonpath_expr = jsonpath_ng.parse(f'*.{k}[{kk}]')
                            res = jsonpath_expr.find(temp)
                            if len(res) == 0:
                                if k not in temp:
                                    try:
                                        temp[k] = []
                                        temp = temp[k]
                                    except:
                                        temp[len(temp) - 1][k] = []
                                        temp = temp[k]
                                else:
                                    # if type(kk) == int and type(temp[k]) == list:
                                    #     if len(temp[k]) == kk:

                                    try:
                                        temp = temp[k][kk]
                                    except:
                                        temp[k].append({})
                                        temp = temp[k][len(temp[k]) - 1]
                            #       print("@@@", k, kk, temp)
                            # else:print("--------")
                    else:
                        jsonpath_expr = jsonpath_ng.parse(f'*.{k}.{kk}')
                        res = jsonpath_expr.find(temp)
                        if len(res) == 0:
                            try:
                                if not isinstance(temp, list):
                                    if k not in temp:
                                        temp[k] = {}
                                    else:
                                        pass
                                    temp = temp[k]
                                else:
                                    if k not in temp[len(temp) - 1]:
                                        temp[len(temp) - 1][k] = {1: 1}
                                    else:
                                        pass
                                    temp = temp[len(temp) - 1][k]
                            except:
                                print("Error")
                        else:
                            print("------")
                else:

                    if isinstance(temp, list):
                        if i >= 1:
                            key = keys[i - 1]
                            try:
                                prev_key = ast.literal_eval(key)
                            except:
                                prev_key = key

                            if isinstance(prev_key, int):
                                if len(temp) - 1 < prev_key:
                                    # print(temp)
                                    temp.append({k: value})

                                else:
                                    temp[prev_key][k] = value
                                    print(f"=====> {value}")
                        else:
                            temp.append({k: value})
                    else:
                        # print(k, value)
                        # print(temp)
                        # print("-----------")
                        temp[k] = value
        else:
            obj[key] = value
    return obj


class AgentModel(object):
    topic: str
    topic_json: dict
    context = {}
    model_info = {}
    model = None
    data = None

    def __init__(self, context, payload = None):
        try:
            if payload:
                self.data = payload #pickle.loads(payload)
            self.context = context
        except:
            traceback.print_exc()

    def find_model(self):
        model = None
        try:
            self.responseModel = dbGateway.getGraphModel_simple(self.context)
            if self.responseModel:
                self.model = self.responseModel.model
            model_size = sys.getsizeof(self.model)
            print(f" Model Size: {model_size} {type(self.model)}", flush=True)
            model =  self.model
        except:
            print(traceback.format_exc(), flush=True)
        return model

    def save_model(self):
        try:
            model_db_meta = {"model_db_host": IP_DB, "model_db_port": PORT_DB}
            response = dbGateway.setGraphModel(self.context, self.data, model_db_meta)
            return response
        except:
            traceback.print_exc()
            print(sys.exc_info(), flush=True)
            return None

    def delete_model(self):
        try:
             return dbGateway.deleteGraphModel(self.context)
        except Exception:
            print(traceback.format_exc(), flush=True)
            return ""

def get_graph():
    return dbGateway.getGraphs()

def get_graph_size(graph_id=None):
        if graph_id is None:
            result = []
            list_graph = get_graph()
            for graph_name in list_graph:
                id = graph_name.replace('graph-node-', '')
                result.append(get_graph_size(id))
        else:
            graph_size = dbGateway.getGraphSize(graph_id)
            graph_models_size = dbGateway.getGraphModelsSize(graph_id)
            result = {
                "name": f"graph-node-{graph_id}",
                "size":
                    {
                        "value": (graph_size + graph_models_size),
                        "details": {
                            "graph": graph_size,
                            "models": graph_models_size
                        }
                    }
            }
        return result






