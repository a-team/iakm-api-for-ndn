#!/bin/bash

###################################### 1- run graphdb image ###################################################################
sudo docker run -d -p 127.0.0.1:7200:7200  --name iakm-graphdb -t khaller/graphdb-free:10.0.0                                 #
sleep 10 # wait to start GraphDB server                                                                                       #
# Loop for all graphs #########################################################################################################
for (( c=1; c<=9; c++ ))                                                                                                      #
do                                                                                                                            #
  new_graph="graph-node-$((c))"                                                                                               #
  #create repository                                                                                                          #
  sed -i "s/graph-node/$new_graph/g" graph-config.ttl                                                                         #
  curl -X POST http://127.0.0.1:7200/rest/repositories -H 'Content-Type: multipart/form-data' -F "config=@graph-config.ttl"   #
  sed -i "s/$new_graph/graph-node/g" graph-config.ttl                                                                         #
  # import graph data #########################################################################################################
  curl -X PUT  "http://127.0.0.1:7200/repositories/$new_graph/statements" -H 'Content-Type: application/x-trig' -H 'Accept: application/json' -d @statements.trigs 
  # add Lucene connectors #####################################################################################################
  python graph-add-connectors.py --node $c  # add Lucene search to graph                                                      #
done                                                                                                                          #
###############################################################################################################################


###################################### 2- run mongodb image ###################################################################
sudo docker run -d -p 127.0.0.1:27017:27017 --expose 27017 --name iakm-mongodb -t mongo:3.6                                   #
###############################################################################################################################


###################################### 3- IAKM-API  ###########################################################################
sudo docker build -t iakm_api .                                                                                               #
IP_GRAPH=$(sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' iakm-graphdb)                    #
IP_DB=$(sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' iakm-mongodb)                       #
sudo docker run -p 5000:5000 -e IP_GRAPH=${IP_GRAPH} -e IP_DB=${IP_DB} --name iakm-api iakm_api                               #
###############################################################################################################################








